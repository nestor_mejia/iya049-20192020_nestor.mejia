const reduce = (fn, init, xs) => {
  
};

const assert = require("assert");

assert.equal(reduce((acc, x) => acc + x, 0, [1, 2, 3]), 6);

assert.deepStrictEqual(
  reduce((acc, x) => [...acc, ...x], [], [[1, 2], [3, 4]]),
  [1, 2, 3, 4]
);

console.log("🎉🎉🎉🎉");
