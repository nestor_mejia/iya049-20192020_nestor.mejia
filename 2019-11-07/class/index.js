const Either = class {

    //Parent class constructor
    constructor(word) {
        //Word that distinguishes if a left or right member should be created
        const AVOID = "Cannot";

        if (word.startsWith(AVOID)) {
            this.Left = word;
        } else {
            this.Right = word;
        }

    }

    //Function which returns the object if defined or an error message if not
    map(fn) {

        if (this.Left !== undefined) {
            return `Left("${this.Left}")`;
        } else {
            return `Right("${fn(this.Right)}")`;
        }

    }

    //Function that receives two functions: errFn is executed if there's no right member, fn if it is
    runEither(errFn, fn) {

        if (this.Left !== undefined) {
            errFn(this.Left);
        } else {
            fn(this.Right);
        }

    }

    //Returns parent type
    getType() {
        return "Either";
    }

}

const createLeft = class extends Either {

    constructor(msg) {
        //Calling parent constructor
        super(msg);
    }

}

const createRight = class extends Either {

    constructor(property) {
        //Calling parent constructor
        super(property);
    }

}

module.exports = {createLeft: createLeft, createRight : createRight};