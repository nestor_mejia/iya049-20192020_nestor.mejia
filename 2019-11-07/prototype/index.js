//Parent object
function Either(word) {
    //Word that distinguishes if a left or right member should be created
    const AVOID = "Cannot";

    if (word.startsWith(AVOID)) {
        this.Left = word;
    } else {
        this.Right = word;
    }

}

//The following functions are being attached to the Either constructor via the prototype, this improves performance

//Function which returns the object if defined or an error message if not
Either.prototype.map = function (fn) {

    if (this.Left !== undefined) {
        return `Left("${this.Left}")`;
    } else {
        return `Right("${fn(this.Right)}")`;
    }

}

//Function that receives two functions: errFn is executed if there's no right member, fn if it is
Either.prototype.runEither = function (errFn, fn) {

    if (this.Left !== undefined) {
        errFn(this.Left);
    } else {
        fn(this.Right);
    }

}

//Returns parent type
Either.prototype.getType = function () {
    return "Either";
}

function createLeft(msg) {
    //Calling parent constructor
    Either.call(this, msg);
}
//Linking parent prototype to child, otherwise we would be unable to access parent properties and methods
createLeft.prototype = Object.create(Either.prototype);

function createRight(property) {
    //Calling parent constructor
    Either.call(this, property)
}
//Linking parent prototype to child, otherwise we would be unable to access parent properties and methods
createRight.prototype = Object.create(Either.prototype);

module.exports = {createLeft: createLeft, createRight : createRight};