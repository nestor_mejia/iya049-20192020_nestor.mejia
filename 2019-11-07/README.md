# Ejercicio 11-07

El ejercicio consiste en implementar una de estas abstracciones:

- List
- Tree
- Maybe
- Either

## Fecha de entrega

El _pull request_ para la entrega del ejercicio debe crearse antes del comienzo de la clase del jueves día 28 de noviembre.

## Enunciado

Modifica el archivo `index.js` de cada carpeta para implementar una de las abstracciones propuestas, utilizando patrones diferentes en cada una de ellas:

- `class/`: debe exponer una clase, creada con el _keyword_ `class`.
- `factory/`: debe exponer una función que pueda ser llamada sin `new`.
- `prototype/`: debe exponer un constructor (implementado sin el _keyword_ `class`).

Modifica el archivo `package.json` de cada carpeta como sea necesario y publica una librería en NPM a partir de cada uno de ellos. Los nombres de los paquetes deben tener un formato acorde a los ejemplos en los párrafos siguientes.

Podéis seguir la guía de NPM sobre cómo publicar un módulo: https://docs.npmjs.com/creating-and-publishing-scoped-public-packages . Tened en cuenta que necesitaréis instalar NPM como ejecutable y crea una cuenta cuyo nombre de usuario sea vuestro código en la asignatura (eg gerardo.munguia).

### List

```javascript
const { Cons, Nil } = require("@gerardo.munguia/list-class");

const food = new Cons("broccoli", new Cons("kale", new Nil()));

console.log(food.head); // -> broccoli
console.log(food.map(meal => `steamed ${meal}`)); // -> ("steamed broccoli", ("steamed kale", nil))
console.log(food.getType()); // -> List
console.log(new Nil().getType()); // -> List
```

### Tree

```javascript
const { Branch, Leaf } = require("@gerardo.munguia/tree-prototype");

const leftBranch = new Branch("foo", new Leaf(), new Leaf());
const rightBranch = new Branch("bar", new Leaf(), new Leaf());
const tree = new Branch("baz", leftBranch, rightBranch);

console.log(tree.left); // -> ("foo", 🍂, 🍂)
console.log(tree.map(word => `${word}!`)); // -> ("baz", ("foo!", 🍂, 🍂), ("bar!", 🍂, 🍂))
console.log(tree.getType()); // -> Tree
console.log(new Leaf().getType()); // -> Tree
```

### Maybe

```javascript
const { createJust, createNothing } = require("@gerardo.munguia/maybe-factory");

const prop = (key, object) =>
  key in object ? createJust(object[key]) : createNothing();

const httpPostMessage = { method: "POST", body: "foobar" };
const httpOptionMessage = { method: "POST" };

console.log(prop("foo", { foo: "bar" }).map(word => `${word}!`)); // -> Just("foo!")
console.log(prop("baz", { foo: "bar" }).map(word => `${word}!`)); // -> Nothing
console.log(prop("foo", { foo: "bar" }).getType()); // -> Maybe
console.log(prop("baz", { foo: "bar" }).getType()); // -> Maybe
```

### Either

```javascript
const { createLeft, createRight } = require("@gerardo.munguia/either-factory");

const prop = (key, object) =>
  key in object
    ? createRight(object[key])
    : createLeft(`Cannot read property '${key}'`);

const foobar = prop("foo", { foo: "bar" });
const bazbar = prop("baz", { foo: "bar" });

console.log(foobar.map(word => `${word}!`)); // -> Right("foo!")
console.log(bazbar.map(word => `${word}!`)); // -> Left("Cannot read property 'baz'")
foobar.runEither(error => console.error(new Error(error)), console.log); // -> foo
bazbar.runEither(error => console.error(new Error(error)), console.log); // -> Error: "Cannot read property 'baz'" at ...
console.log(prop("foo", { foo: "bar" }).getType()); // -> Either
console.log(prop("baz", { foo: "bar" }).getType()); // -> Either
```

## Tecnologías

El ejercicio se debe realizar sin utilizar librerías externas.

## Entregables

Todos los archivos de esta carpeta son entregrables.

El método de entrega será:

- Un _pull request_ a su rama _master_ (`/alumno/xxx.yyy/master`). El PR puede hacerse desde un _fork_ si es preciso.
- Tres paquetes publicados en NPM, como se indica en el enunciado.

Preferiblemente, cread la rama para esta práctica a partir de la rama `master` del repositorio de la asignatura.
