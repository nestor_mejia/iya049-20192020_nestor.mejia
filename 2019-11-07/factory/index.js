//Parent object
const Either = (word) =>{

    //We'll return this object
    const either = {};

    //Word that distinguishes if a left or right member should be created
    const AVOID = "Cannot";

    if (word.startsWith(AVOID)) {
        either.Left = word;
    } else {
        either.Right = word;
    }

    //Function which returns the object if defined or an error message if not
    either.map = (fn) => {

        if (either.Left !== undefined) {
            return `Left("${either.Left}")`;
        } else {
            return `Right("${fn(either.Right)}")`;
        }

    }

    //Function that receives two functions: errFn is executed if there's no right member, fn if it is
    either.runEither = (errFn, fn) => {

        if (either.Left !== undefined) {
            errFn(either.Left);
        } else {
            fn(either.Right);
        }

    }

    //Returns parent type
    either.getType = () => {
        return "Either";
    }

    return either;

}

const createLeft = (msg) =>{
    //"Calling" constructor
    const left = Either(msg);
    //We could add more porperties if we wanted
    return left;

}

const createRight = (property) =>{
    //"Calling" constructor
    const right = Either(property);
    //We could add more porperties if we wanted
    return right;

}

module.exports = {createLeft: createLeft, createRight : createRight};