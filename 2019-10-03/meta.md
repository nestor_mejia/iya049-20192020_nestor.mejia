## Integrantes del grupo.

... ✏️
* Alvarena Mairena, Patricia Guadalupe
* Ardón Ramos, Kevin Eduardo
* Díez Fernández, Sergio
* Mejía Cañas, Nestor Ulises

## Desglose de tareas

* Implementar la `creación de villanos`. 

 Asignado a Mejía Cañas, Nestor Ulises

* Implementar el `movimiento de villanos`.

 Asignado a Mejía Cañas, Nestor Ulises

* Implementar la `lógica de colisiones laterales de villanos`

 Asignado a Mejía Cañas, Nestor Ulises

* Implementar la `creación del héroe`

 Asignado a Ardón Ramos, Kevin Eduardo

* Implementar el `movimiento del héroe`

 Asignado a Ardón Ramos, Kevin Eduardo

* Implementar la `lógica de disparo`

 Asignado a Alvarenga Mairena, Patricia Guadalupe

* Implementar `artwork`

 Asignado a Díez Fernández, Sergio
