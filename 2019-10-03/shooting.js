"use strict";
const shoot = (
    positionX) => {

    console.log("disparo");
    
    const shootDiv = document.getElementById("js-shoot");
    const ringDiv = document.getElementById("zonaJuego");
    const heroDiv = document.getElementById("heroe");

    const shootingX = heroDiv.getBoundingClientRect().x;
    const shootingY = heroDiv.getBoundingClientRect().y;

    shootDiv.style.left = shootingX + "px"
    shootDiv.style.top = shootingY + "px";
    shootDiv.style.display = "block"

    let intervalID = window.setInterval(movement, 10);
    let posY = 0;

    function movement() {
        let isOut = isOutOfRing()
        let killingEnemy = isCollidingWithEnemy()
        if (isOut) {
            clearInterval(intervalID);
            shootDiv.style.display = "none"
        } else {
            posY = posY + 1;
            shootDiv.style.bottom = posY + "px";
        }


    }

    const isOutOfRing = () => {
        const ringSizesData = ringDiv.getBoundingClientRect();
        const shootSizesData = shootDiv.getBoundingClientRect();
        let isColliding = false;


        if (ringSizesData.height - (posY + shootSizesData.height) < 0) {
            isColliding = true;
        }
        return isColliding
    }

    const isCollidingWithEnemy = () => {
        //TO-DO Implement enemies colliding logic here
    }
}