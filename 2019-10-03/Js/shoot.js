"use strict";
        const shoot = (positionX) => {
             
            const shootDiv = document.getElementById("js-shoot");
            const ringDiv = document.getElementById("zonaJuego")

            shootDiv.style.left = positionX + "px"
            shootDiv.style.bottom = "0px"
            shootDiv.style.display = "block"

            let intervalID = window.setInterval(movement, 3);
            let posY = 25;

            function movement() {
                let isOut = isOutOfRing()
                let killingEnemy = isCollidingWithEnemy()
                if (isOut) {
                    clearInterval(intervalID);
                    shootDiv.style.display = "none"
                } else {
                    posY = posY + 1;
                    shootDiv.style.bottom = posY + "px";
                }


            }

            const isOutOfRing = () => {
                const ringSizesData = ringDiv.getBoundingClientRect();
                const shootSizesData = shootDiv.getBoundingClientRect();
                let isColliding = false;


                if (ringSizesData.height - (posY + shootSizesData.height) < 0) {
                    isColliding = true;
                }
                return isColliding
            }

            const isCollidingWithEnemy = () => {
                //TO-DO Implement enemies colliding logic here
            }
        }