var direction = true;

window.onload = function () {
    this.generateVillains(40);
    this.moveVillains();
};

function generateVillains(n) {
    var horizontalHandler = 0;
    for (var i = 0; i < n; i++) {
        if ((i % 10) == 0) {
            horizontalHandler = 0;
            const breakLine = document.createElement("br");
            document.getElementById("enemyDiv").appendChild(breakLine);
        }
        
        var villainDiv = document.createElement("div");
        villainDiv.className += 'villain-div';
        villainDiv.id = `villain${i}`

        const villainBg = document.createElement("img");
        villainBg.src = "./images/enemy.png";
        villainBg.className += 'villain-image';

        villainDiv.appendChild(villainBg);
        
        document.getElementById("enemyDiv").appendChild(villainDiv);
        if( (i%10!==0) ){
            horizontalHandler = document.getElementById(`villain${i-1}`).getBoundingClientRect().x;
        }
        
        var space = (horizontalHandler + 10);
        document.getElementById(`villain${i}`).style.left = `${space}px`;
        horizontalHandler += space;
    }
}

function moveVillains() {
    setInterval(moveThemH, 500);
    setInterval(moveThemV, 2500);
    setInterval(()=>{
        direction = !direction;
    },2000);
    // setInterval(thanosFunction, 1000);
}

const moveThemH = () => {
    var villains = document.getElementById("villain0");
    const coords = villains.getBoundingClientRect();
    const movCoord = changeDirection(coords.x);
    document.getElementById("enemyDiv").style.left = movCoord+'px';
}

const moveThemV = () =>{
    var villains = document.getElementById("villain0");
    const coords = villains.getBoundingClientRect();
    const movCoord = coords.y - 225;
    document.getElementById("enemyDiv").style.top = movCoord+'px';
}

const changeDirection = (pixels) =>{
    if (direction){
        return pixels -450;
    } else{
        // debugger;
        return pixels - 475;
    }
}

const thanosFunction = () =>{
    
    try{
        const divToEliminate = (Math.random() * 99) | 0 ;
        document.getElementById(`villain${divToEliminate}`).remove();
    } catch (e){
        console.warn("Villain has already been killed");
    }


   
}